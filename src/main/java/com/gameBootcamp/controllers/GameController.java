package com.gameBootcamp.controllers;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gameBootcamp.dtos.request.GameRequest;
import com.gameBootcamp.dtos.response.GameResponse;
import com.gameBootcamp.services.GameService;

@RestController
public class GameController {  // Las clases RestControlador son solo para gestionar (recibir) las respuestas
	
	@Autowired
	private GameService gameService;
	
	/* Método GET para el end-poing "/game", se le devuelve al front el ID*/
	@GetMapping("/game")
	public ResponseEntity<Object> getGame(@RequestParam("title") String title, HttpServletRequest request){
		return ResponseEntity.status(HttpStatus.OK).body(gameService.getGame(title));
	}
	
	@PostMapping("/game")
	public ResponseEntity<Object> addGame(@RequestBody @Valid GameRequest gameRequest, HttpServletRequest request){
		GameResponse gameResponse = gameService.addGame(gameRequest);
	
	/*
	 * Esto que se hacía antes, se debe hacer mediante un servicio.
	 * Se creó el servicio GameService para ello.
	
	List<GameDto> gamesDtos = new ArrayList<>();
	
	@GetMapping("/genreGame")
	
	public ResponseEntity<Object> getGenreGame(@RequestParam("title") String title, HttpServletRequest request){
		
		Optional<GameDto> gameDto = gamesDtos.stream().filter(g -> g.getTitle().equals(title)).findAny();
		if(gameDto.isPresent())
			return ResponseEntity.status(HttpStatus.OK).body(gameDto.get().getGenre());
		else
			return ResponseEntity.status(HttpStatus.OK).body("No se ha encontrado el juego: ".concat(title));
	}
	*/		
		return ResponseEntity.status(HttpStatus.OK).body("Juego añadido correctamente");
	}
	
	@DeleteMapping("/game")
	public ResponseEntity<Object> deleteGame(@RequestParam("id") Long id, HttpServletRequest request){
		gameService.deleteShop(id);
		return ResponseEntity.status(HttpStatus.OK).body("El juego se ha borrado correctamente");
	}

	
}
