package com.gameBootcamp.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name="SHOPS")
@Data
public class Shop {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="NAME")
	private String name;
	
	@Column(name="ADDRESS")
	private String address;
	
	@Column(name="POSTAL_CODE")
	private Long postalCode;

	@Column(name="SHOP_ID")
	@OneToMany(mappedBy = "shopId", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Storage> storage;
}
