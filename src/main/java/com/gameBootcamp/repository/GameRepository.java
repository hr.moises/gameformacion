package com.gameBootcamp.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gameBootcamp.entities.Game;

@Repository
public interface GameRepository extends JpaRepository<Game, Long> {	

	public Optional<Game> findByTitle(String title);
	
}
