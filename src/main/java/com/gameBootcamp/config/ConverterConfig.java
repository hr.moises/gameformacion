package com.gameBootcamp.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.gameBootcamp.convert.GenreRequestToGenreConverter;
import com.gameBootcamp.convert.game.GameRequestToGameConverter;
import com.gameBootcamp.convert.game.GameToGameResponseConverter;
import com.gameBootcamp.convert.shop.ShopRequestToShopConverter;
import com.gameBootcamp.convert.shop.ShopToShopResponseConverter;

@Configuration
public class ConverterConfig implements WebMvcConfigurer {

	@Override
	public void addFormatters(FormatterRegistry registry) {
		registry.addConverter(new GameRequestToGameConverter());
		registry.addConverter(new GameToGameResponseConverter());
		registry.addConverter(new GenreRequestToGenreConverter());
		registry.addConverter(new ShopRequestToShopConverter());
		registry.addConverter(new ShopToShopResponseConverter());
	}
	
	
}
