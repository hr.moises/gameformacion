package com.gameBootcamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GameBootcampApplication {

	public static void main(String[] args) {
		SpringApplication.run(GameBootcampApplication.class, args);
	}

}
