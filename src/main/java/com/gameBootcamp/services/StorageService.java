package com.gameBootcamp.services;

import com.gameBootcamp.dtos.request.StorageRequest;
import com.gameBootcamp.dtos.response.StorageResponse;

public interface StorageService {

	public StorageResponse addShop(StorageRequest ShopDto);

	public StorageResponse getShop(String name);
	
	public StorageResponse findShop(Long id);

	public void deleteShop(Long id);
}
