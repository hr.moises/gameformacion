package com.gameBootcamp.dtos.request;

import java.util.ArrayList;
import java.util.List;
import java.sql.Date;

import javax.validation.constraints.NotBlank;

import com.gameBootcamp.enums.GenreEnum;

import lombok.Data;

@Data
public class GameRequest {
	
	/* DTO: Objeto de transferencia de datos
	 * Se usa para el envío de objetos entre el front y el back
	 * 
	 * En este caso, GameDto son los datos que envía el front al back cuando se añada un juego
	 */
	
	@NotBlank(message = "El título está sin informar.")
	private String title;
	
	private String description;
	
	private List<GenreEnum> genre = new ArrayList<>();
	
	private Date release;

}