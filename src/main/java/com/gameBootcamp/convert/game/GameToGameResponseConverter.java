package com.gameBootcamp.convert.game;

import org.springframework.core.convert.converter.Converter;

import com.gameBootcamp.dtos.response.GameResponse;
import com.gameBootcamp.entities.Game;

public class GameToGameResponseConverter implements Converter<Game, GameResponse> {

	@Override
	public GameResponse convert(Game game) {
		GameResponse gameResponse = new GameResponse();
		
		gameResponse.setId(game.getId());
		gameResponse.setTitle(game.getTitle());
		gameResponse.setDescription(game.getDescription());
		gameResponse.setDate(game.getDate());
		
		return gameResponse;
	}

	
}
