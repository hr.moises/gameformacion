package com.gameBootcamp.dtos.response;

import lombok.Data;

@Data
public class ShopResponse {
	
	private Long id;
	private String name;
	private String address;
	private Long postalCode;

	public ShopResponse() {
		
	}
}
