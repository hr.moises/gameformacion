package com.gameBootcamp.convert.shop;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

import com.gameBootcamp.dtos.request.ShopRequest;
import com.gameBootcamp.entities.Shop;

public class ShopRequestToShopConverter implements Converter<ShopRequest, Shop> {

	@Override
	public Shop convert(ShopRequest shopRequest) {
		Shop shop = new Shop();

		shop.setName(shopRequest.getName());
		shop.setAddress(shopRequest.getAddress());
		shop.setPostalCode(shopRequest.getPostalCode());
		
		return shop;
	}

}
