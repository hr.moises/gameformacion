package com.gameBootcamp.dtos.response;

import lombok.Data;

@Data
public class StorageResponse {

	private Long gameId;
	private Long shopId;
	private Long quantity;
	
	public StorageResponse() {
		
	}
}
