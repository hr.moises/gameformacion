package com.gameBootcamp.exceptions;

import com.gameBootcamp.exceptions.generic.NotFoundExecption;

public class GameNotFoundException extends NotFoundExecption {

	private static final long serialVersionUID = 1L;
	
	private static final String MESSAGE = "Juego no encontrado";

	public GameNotFoundException(String detalle) {
		super(detalle);
	}

	public GameNotFoundException() {
		super(MESSAGE);
	}
}
