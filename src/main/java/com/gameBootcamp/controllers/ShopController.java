package com.gameBootcamp.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gameBootcamp.dtos.request.ShopRequest;
import com.gameBootcamp.dtos.response.ShopResponse;
import com.gameBootcamp.services.ShopService;

@RestController
public class ShopController {
	
	@Autowired
	private ShopService shopService;

	@GetMapping("/shop")
	public ResponseEntity<Object> getShop(@RequestParam("name") String name, HttpServletRequest request){
		return ResponseEntity.status(HttpStatus.OK).body(shopService.getShop(name));
	}
	
	@PostMapping("/shop")
	public ResponseEntity<Object> addShop(@RequestBody @Valid ShopRequest shopRequest, HttpServletRequest request){
		ShopResponse shopResponse = shopService.addShop(shopRequest);

		return ResponseEntity.status(HttpStatus.OK).body("Tienda añadida correctamente");
	}
	
	@DeleteMapping("/shop")
	public ResponseEntity<Object> deleteShop(@RequestParam("id") Long id, HttpServletRequest request){
		shopService.deleteShop(id);
		return ResponseEntity.status(HttpStatus.OK).body("La tienda se ha borrado correctamente");
	}

}
