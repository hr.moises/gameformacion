package com.gameBootcamp.entities;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name="GAMES")
@Data // Para no crear los getters y setters
public class Game {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name="TITLE")  // Si no se pone, utiliza por defecto el nombre del atributo
	private String title;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	@Column(name="DATE")
	private Date date;

	
	// Relacionamos la tabla juegos con la categoría
	//@ManyToMany(cascade = CascadeType.ALL)
	@ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	private List<Genre> genres = new ArrayList<>();
	/*
	 * CascadeType.PERSIST: Cuando una entidad es persistida, su entidad relacionada debe ser persistida también. 
	 * CascadeType.REMOVE:  Cuando una entidad es removida, su entidad relacionada debe ser removida también.
	 * CascadeType.REFRESH: Cuando una entidad es refrescada, su entidad relacionada debe ser refrescada también.
	 * CascadeType.MERGE:   Cuando una entidad es actualizada, su entidad relacionada debe ser actualizada también.
	 * CascadeType.ALL:     Cuando una entidad es persistida, removida, refrescada o actualizada, 
	 *                      su entidad relacionada debe ser persistida, removida, refrescada o actualizada también. 
	 */
	
	
	// Relacionamos la tabla juegos con el almacén
	//@OneToMany(mappedBy = "game")
	@Column(name="GAME_ID")  // Es el nombre que cogerá la el campo en la tabla STORAGES
	@OneToMany(mappedBy = "gameId", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Storage> storage;
	/* NOTA: Con orphanRemoval lo que hacemos es que si se "desconecta" una relación, se elimina de la tabla
	 * Por ejemplo, si un juego deja de estar en una tienda, se eliminaría el stock de ese juego para la tienda
	 */
}
