package com.gameBootcamp.services.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import com.gameBootcamp.controllers.GameController;
import com.gameBootcamp.dtos.request.ShopRequest;
import com.gameBootcamp.dtos.response.GameResponse;
import com.gameBootcamp.dtos.response.ShopResponse;
import com.gameBootcamp.entities.Shop;
import com.gameBootcamp.entities.Shop;
import com.gameBootcamp.exceptions.GameKONotFoundException;
import com.gameBootcamp.exceptions.generic.NoContentExecption;
import com.gameBootcamp.helper.ShopHelper;
import com.gameBootcamp.repository.ShopRepository;
import com.gameBootcamp.services.ShopService;

@Service
public class ShopServiceImpl implements ShopService {
	
	private static final Logger logger = LoggerFactory.getLogger(GameController.class);
	
	@Autowired
	private ShopHelper shopHelper;
	
	@Autowired
	private ShopRepository shopRepository;
	
	@Autowired
	private ConversionService converter;

	@Override
	public ShopResponse addShop(ShopRequest shopRequest) {
		Shop shop = shopHelper.convertShopRequestToShop(shopRequest);
		
		shopRepository.save(shop);
		
		logger.info("Se ha insertado registro en la tabla SHOPS");
		
		return new ShopResponse();
	}

	@Override
	public ShopResponse getShop(String name) {

		Optional<Shop> shop = shopRepository.findByName(name);

		if(shop.isPresent())
			return converter.convert(shop.get(), ShopResponse.class);
		else // Habría que crear el hadler para tienda, me falta tiempo...
			throw new GameKONotFoundException("Tienda no encontrada");
	}

	@Override
	public ShopResponse findShop(Long id) {
		Optional<Shop> shop = shopRepository.findById(id);

		if(shop.isPresent())
			return converter.convert(shop.get(), ShopResponse.class);
		else // Habría que crear el hadler para tienda, me falta tiempo...
			throw new GameKONotFoundException("Tienda no encontrada");
	}

	@Override
	public void deleteShop(Long id) {
		shopRepository.deleteById(id);
		logger.info("Se ha elimnado el registro en la tabla SHOPS con el ID=" + id);
	}

}
