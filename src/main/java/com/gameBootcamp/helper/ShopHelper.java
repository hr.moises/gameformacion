package com.gameBootcamp.helper;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import com.gameBootcamp.convert.shop.ShopRequestToShopConverter;
import com.gameBootcamp.dtos.request.ShopRequest;
import com.gameBootcamp.entities.Shop;
import com.gameBootcamp.exceptions.generic.NoContentExecption;

@Service
public class ShopHelper {
	
	@Autowired
	private ConversionService converter;
	
	/*@Autowired
	private ShopRequestToShopConverter shopRequestToShopConverter;*/
	
	
	// Si la tienda está presente la devuelve, sino envía mensaje error personalizado
	public ShopRequest getIfExistShop(Optional<ShopRequest> shopDto) {
		if (shopDto.isPresent())
			return shopDto.get();
		else {
			//throw new GameKOException("Juego no encontrado");
			throw new NoContentExecption("Tienda no encontrada");
		}
		
	}

	// Convierte la "pregunta" (post) en la clase tienda
	public Shop convertShopRequestToShop(ShopRequest shopRequest) {

		Shop shop = converter.convert(shopRequest, Shop.class);
		//Shop shop = shopRequestToShopConverter.convert(shopRequest);
		
		return shop;
	}
}
