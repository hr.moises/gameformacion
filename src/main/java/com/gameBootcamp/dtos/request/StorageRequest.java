package com.gameBootcamp.dtos.request;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class StorageRequest {
	
	@NotBlank(message = "ID juego sin informar.")
	private Long gameId;
	
	@NotBlank(message = "ID tienda sin informar.")
	private Long shopId;
	
	private Long quantity;
}
