package com.gameBootcamp.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gameBootcamp.entities.Storage;
import com.gameBootcamp.entities.ids.StorageId;

@Repository
public interface StorageRepository extends JpaRepository<Storage, StorageId> {
	
	public Optional<Storage> findByQuantity(Long q);

}
