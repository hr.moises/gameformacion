package com.gameBootcamp.helper;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import com.gameBootcamp.dtos.request.GameRequest;
import com.gameBootcamp.entities.Game;
import com.gameBootcamp.entities.Genre;
import com.gameBootcamp.exceptions.generic.GameKOException;

@Service
public class GameHelper {
	
	@Autowired
	private ConversionService converter;
	
	public GameRequest getIfExistGame(Optional<GameRequest> gameDto) {
		if (gameDto.isPresent())
			return gameDto.get();
		else
			throw new GameKOException("Juego no encontrado");
	}
	
	public Game convertGameRequestToGame(GameRequest gameRequest) {
		Game game = converter.convert(gameRequest, Game.class);
		List<Genre> genres = gameRequest.getGenre().stream().map(gr -> converter.convert(gr, Genre.class)).collect(Collectors.toList());
		game.getGenres().addAll(genres);
		return game;
	}

}
