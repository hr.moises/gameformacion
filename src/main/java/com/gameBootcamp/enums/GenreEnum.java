package com.gameBootcamp.enums;

public enum GenreEnum {

	/*
	 * Con el enum controlamos que no se introduzcan categorías de juegos que no están en la lista
	 */
	ADVENTURE,ACTION,FIGHT,TERROR,ARCADE,RETRO
}
