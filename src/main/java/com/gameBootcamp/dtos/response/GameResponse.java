package com.gameBootcamp.dtos.response;

import java.sql.Date;

import lombok.Data;

@Data
public class GameResponse {
	
	private Long id;
	private String title;
	private Date date;
	private String description;

	public GameResponse () {
		
	}
}
