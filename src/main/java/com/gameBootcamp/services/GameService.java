package com.gameBootcamp.services;

import com.gameBootcamp.dtos.request.GameRequest;
import com.gameBootcamp.dtos.response.GameResponse;

public interface GameService {
	
	public GameResponse addGame(GameRequest gameDto);

	public GameResponse getGame(String title);
	
	public GameResponse getId(Long id);

	public void deleteShop(Long id);
}
