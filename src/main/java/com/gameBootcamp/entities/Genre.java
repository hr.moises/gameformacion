package com.gameBootcamp.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.gameBootcamp.enums.GenreEnum;

import lombok.Data;

@Entity
@Table(name="GENRES")
@Data 
public class Genre {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="GENRE_NAME")
	@Enumerated(EnumType.STRING)  // Para que en la table se muestre los nombres de las categorías
	private GenreEnum genreName;
	
	@ManyToMany(mappedBy = "genres")
	private List<Game> games;
}
