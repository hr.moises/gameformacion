package com.gameBootcamp.convert;

import org.springframework.core.convert.converter.Converter;

import com.gameBootcamp.dtos.response.GameResponse;
import com.gameBootcamp.entities.Genre;
import com.gameBootcamp.enums.GenreEnum;

public class GenreRequestToGenreConverter implements Converter<GenreEnum, Genre> {

	@Override
	public Genre convert(GenreEnum genreEnum) {
		Genre genre = new Genre();
		genre.setGenreName(genreEnum);
		return genre;
	}

}
