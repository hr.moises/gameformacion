package com.gameBootcamp.services;

import com.gameBootcamp.dtos.request.ShopRequest;
import com.gameBootcamp.dtos.response.ShopResponse;

public interface ShopService {
	
	public ShopResponse addShop(ShopRequest ShopDto);

	public ShopResponse getShop(String name);
	
	public ShopResponse findShop(Long id);

	public void deleteShop(Long id);
}
