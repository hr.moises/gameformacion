package com.gameBootcamp.exceptions;

import com.gameBootcamp.exceptions.generic.GameKOException;

public class GameKONotFoundException extends GameKOException {

	private static final long serialVersionUID = 1L;
	
	private static final String MESSAGE = "Juego no encontrado";
	
	public GameKONotFoundException() {
		super(MESSAGE);
	}
	
	public GameKONotFoundException(String detalle) {  // Para hacer un throw con un mensaje personalizado
		super(detalle);
	}

}
