package com.gameBootcamp.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.gameBootcamp.entities.ids.StorageId;

import lombok.Data;

@Data
@Entity
@Table(name="STORAGES")
@IdClass(StorageId.class)
public class Storage {

	/*@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;*/
	
	@Column(name="QUANTITY")
	private Long quantity;

	@Id
	@ManyToOne()
    @JoinColumn(name = "gameId")
	//@JoinColumn(foreignKey = @ForeignKey(name = "game_id"))
	//@JoinColumn(name = "game_id", referencedColumnName = "id")
    private Game gameId;

	@Id
	@ManyToOne()
    @JoinColumn(name = "shopId")
    private Shop shopId;
}
