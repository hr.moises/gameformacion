package com.gameBootcamp.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import com.gameBootcamp.helper.GameHelper;
import com.gameBootcamp.repository.GameRepository;
import com.gameBootcamp.controllers.GameController;
import com.gameBootcamp.dtos.request.GameRequest;
import com.gameBootcamp.dtos.response.GameResponse;
import com.gameBootcamp.entities.Game;
import com.gameBootcamp.exceptions.GameKONotFoundException;
import com.gameBootcamp.services.GameService;

@Service
public class GameServiceImpl implements GameService {
	
	// Declaramos la variable para el log
	private static final Logger logger = LoggerFactory.getLogger(GameController.class);

	@Autowired
	private GameHelper gameHelper;
	
	@Autowired
	private GameRepository gameRepository;
	
	@Autowired
	private ConversionService converter;
	
	List<GameRequest> gamesRequest = new ArrayList<>();
	
	@Override
	public GameResponse addGame(GameRequest gameRequest) {
		// gamesRequest.add(gameRequest);
		//Game game = GameConverter.DtoToEntity(gameRequest);
		Game game = gameHelper.convertGameRequestToGame(gameRequest);
		gameRepository.save(game);
		logger.info("Se ha insertado registro en la tabla GAMES");
		return new GameResponse();
	}

	@Override
	public GameResponse getGame(String title) {
		//Optional<GameRequest> gameRequest = gamesRequest.stream().filter((g -> g.getTitle().equals(title))).findAny();
		
		/* Este código se pasa al helper, para limpiar al clase y esté todo en su sitio 
		 * 
		if (gameDto.isPresent()) 
		 
			return gameDto.get();
		else
			throw new GameKOException("Juego no encontado");
		*/
		
		// return gameHelper.getIfExistGame(gameRequest);
		
		Optional<Game> game = gameRepository.findByTitle(title);

		if(game.isPresent())
			return converter.convert(game.get(), GameResponse.class);
		else 
			throw new GameKONotFoundException();
		
		/*GameRequest gameRequest = GameConverter.EntityToDto(game.get());	
		return GameResponse;*/
		
	}

	@Override
	public GameResponse getId(Long id) {
		Optional<Game> game = gameRepository.findById(id);

		if(game.isPresent())
			return converter.convert(game.get(), GameResponse.class);
		else // Habría que crear el hadler para tienda, me falta tiempo...
			throw new GameKONotFoundException("Juego no encontrado");
	}

	@Override
	public void deleteShop(Long id) {
		gameRepository.deleteById(id);
		logger.info("Se ha elimnado el registro en la tabla GAMES con el ID=" + id);
	}

}
