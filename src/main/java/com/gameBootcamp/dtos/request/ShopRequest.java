package com.gameBootcamp.dtos.request;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class ShopRequest {

	@NotBlank(message = "El nombre de la tienda está sin informar.")
	private String name;
	
	private String address;
	
	private Long postalCode;
	
}