package com.gameBootcamp.entities.ids;

import java.io.Serializable;


import com.gameBootcamp.entities.Game;
import com.gameBootcamp.entities.Shop;

import lombok.Data;

@Data
public class StorageId implements Serializable {

	private Game gameId;
	private Shop shopId;
    
    public StorageId (Game gameId, Shop shopId) {
    	this.gameId = gameId;
    	this.shopId = shopId;
    }    
    
}
