package com.gameBootcamp.convert.shop;

import org.springframework.core.convert.converter.Converter;

import com.gameBootcamp.dtos.response.ShopResponse;
import com.gameBootcamp.entities.Shop;

public class ShopToShopResponseConverter implements Converter<Shop, ShopResponse> {

	@Override
	public ShopResponse convert(Shop shop) {
		ShopResponse shopResponse = new ShopResponse();
		
		shopResponse.setId(shop.getId());
		shopResponse.setName(shop.getName());
		shopResponse.setAddress(shop.getAddress());
		shopResponse.setPostalCode(shop.getPostalCode());
		
		return shopResponse;
	}

}
