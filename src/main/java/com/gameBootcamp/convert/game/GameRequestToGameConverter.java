package com.gameBootcamp.convert.game;

import org.springframework.core.convert.converter.Converter;

import com.gameBootcamp.dtos.request.GameRequest;
import com.gameBootcamp.entities.Game;

public class GameRequestToGameConverter implements Converter<GameRequest, Game> {

	@Override
	public Game convert(GameRequest gameRequest) {
		Game game = new Game();

		game.setTitle(gameRequest.getTitle());
		game.setDescription(gameRequest.getDescription());
		game.setDate(gameRequest.getRelease());
		
		return game;
	}

}
